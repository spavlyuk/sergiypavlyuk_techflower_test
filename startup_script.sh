#!/bin/bash

DB_USER=test
DB_PSW=testtest

if [ ! -z "$1" ]; then
    echo rebuilding containers and running docker compose
    docker-compose up -d --build
else
    echo trying to run docker-compose without explicit rebuilding of containers
    docker-compose up -d
fi


echo 
echo
echo "setting up database"
sleep 3
docker exec db-server influx setup --org my-org --bucket price-bucket --username $DB_USER --password $DB_PSW --force

sleep 1
echo getting new credentials from db-server
docker exec db-server bash -c "influx auth list --user test --json > /auth.json"

sleep 2
echo move the credentials from db-server to local folder
docker cp db-server:/auth.json $(pwd)/auth.json

sleep 2
docker cp $(pwd)/auth.json reporting-server:/app/auth.json
docker cp $(pwd)/set_e_vars.py reporting-server:/app/set_e_vars.py
docker-compose exec reporting-server python3 set_e_vars.py

sleep 2
docker cp $(pwd)/auth.json beat-server:/app/auth.json
docker cp $(pwd)/set_e_vars.py beat-server:/app/set_e_vars.py
docker-compose exec beat-server python3 set_e_vars.py

docker cp $(pwd)/auth.json worker-server:/app/auth.json
docker cp $(pwd)/set_e_vars.py worker-server:/app/set_e_vars.py
docker-compose exec worker-server python3 set_e_vars.py

sleep 2
echo "cleaning up local machine"
rm $(pwd)/auth.json

echo restarting some services
sleep 3
docker-compose restart beat-server
sleep 3
docker-compose restart reporting-server
sleep 3
docker-compose restart worker-server



#export to reporting-server

#export to beat-server

#export to worker-server

#remove file from home folder

# restart some services

# echo "ready to go"
