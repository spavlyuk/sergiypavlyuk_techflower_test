import os
import json

def return_broker():
    try:
        broker = os.environ['CELERY_BROKER_URL'],
    except KeyError:
        broker = "amqp://rabbitmq:5672"
    
    return broker

def return_db_url():
    db_url = "http://db-server:8086"
    return db_url

def return_org():
    try:
        org = os.environ['DB_SERVER_ORG']
    except KeyError:
        org = "my-org"

    return org

# try:
#     token = os.environ['DB_SERVER_TOKEN']
# except KeyError:
#     token = "not found"

def return_token():
    try:
        with open('/app/auth.json') as f:
            db_creds = json.load(f)
            token = db_creds[0]['token']
            # os.environ['DB_SERVER_TOKEN'] = token
    except FileNotFoundError:
        print ('file with token not found')
        token = os.environ['DB_SERVER_TOKEN']
        # token = '9G0RC_MkbHAY0aO_FkA8vDqIbZsWgE_K3iX1PP3O1jXe8rg7c2aKylTv8fQVBzzS_6_IHJBBq3OFLTmdDcy1gA=='
    return token




