from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS, PointSettings
import pandas as pd
import datetime 
from config import *

def read_data(fname:str = 'data.csv'):

    df = pd.read_csv(fname)

    # since delay is always slightly more than 10 seconds, ts overlapping
    # will not happen if we convert Seconds to current time values

    # in
    time_now = datetime.datetime.now()
    df['timestamp'] = [time_now - datetime.timedelta(seconds = 10-i) for i in df['Second']]
    df['timestamp'] = pd.to_datetime(df['timestamp'])
    df['timestamp'] = [i.tz_localize('EET') for i in df['timestamp']]
    df = df.set_index('timestamp')
    
    send_data_to_db(df)


def send_data_to_db(df:pd.DataFrame):

    client = InfluxDBClient(url=return_db_url(),
                            org=return_org(),
                            token=return_token(),
                            debug=True)
    

    point_settings = PointSettings(**{"type":"stock-data"})
    point_settings.add_default_tag("example-name", "ingest-data-frame")

    write_api = client.write_api(
                                write_options=SYNCHRONOUS, 
                                point_settings=point_settings
                                )
    write_api.write(
            bucket="price-bucket", org="my-org",
            record=df, data_frame_measurement_name="stock-prices-df",
    )
