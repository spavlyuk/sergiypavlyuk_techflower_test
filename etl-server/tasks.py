from celery import Celery
from data_manager import read_data
import os
from config import *

broker = return_broker()
    
app = Celery(broker=broker)

@app.task
def check(*args):
    read_data()


@app.on_after_configure.connect
def setup_request(sender, **kwargs):
    sender.add_periodic_task(10.0, check.s(), name='check data.csv file')
