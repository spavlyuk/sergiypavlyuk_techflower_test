# Sergiy's solution for Techflower Test Task

The following solution has been tested on Ubuntu 20 with Python 3.8

## Set up, install and run
1. clone the repository

``` git clone https://spavlyuk@bitbucket.org/spavlyuk/sergiypavlyuk_techflower_test.git ``` 

2. make sure you have latest docker and docker-compose installed
3. run `startup_script.sh --build`. this will build containers and will set everythin up. tag `--build` is only needed when there is need to rebuild containers
4. goto http://localhost:8050 to view the dashboard


## The structure of the solution

Docker-compose spins up several containers under one bridge network

- db-server - standard influxdb, furthermore the script creates a test user with (pswd: testtest), organisation and a bucket
- beat-server - celery service with periodic tasks scheduler
- worker-server - celery worker that loads, formats and uploads data to the db
- reportings-server - simple (no production) dashboard that pools new data from the db every 10 seconds and displays the price chart

The script copies the token from db-server to other containers and restarts them so they can automatically connect to the db

## What I would do more
- limit resource usage by Containers
- put reporting-server in production (gunicorn & nginx)
- one admin user for the database
- the data pull takes last 30 mins of data from the server, however displays only last 200 datapoints. Need to 
- function and variable annotations, it's still a mess) 

## Conclusion

Having spent around 10 hours on this solution, including learning new technologies and bug fixing (env_variables did not work they way I expected). But in any case, thank you for giving an opportunity to try something new). Anyway, further effort takes me to the situation where I will spend 80% of my total effort to reach 20% of improvement. Just wanted to say that I could do more, but as usually decided to cut the amount of time I spend on this task
