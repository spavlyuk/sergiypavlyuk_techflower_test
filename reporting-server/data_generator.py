from influxdb_client import InfluxDBClient, Dialect
import pandas as pd
from config import *

db_url = return_db_url()
org = return_org()
token = return_token()

def get_data(zoom_last_values = zoom_last_values):
    '''this function polls the ts server
    and gets the info back
    '''
    

    client = InfluxDBClient(url=db_url,
                            org=org,
                            token=token,
                            debug=False)

    query_api = client.query_api()
    
    stream = query_api.query_stream( 
                                    """from(bucket: "price-bucket")
                                    |> range(start: -30m)
                                    |> filter(fn: (r) => r["_measurement"] == "stock-prices-df")
                                    |> filter(fn: (r) => r["_field"] == "Stock_value")
                                    """
    )
    df = []
    for res in stream:
        df.append( {'timestamp':res['_time'], 'stock_value': res['_value']})

    stream.close()
    df = pd.DataFrame(df)
    df['timestamp'] = pd.to_datetime(df['timestamp'])
    df['timestamp'] = [i.tz_convert('EET') for i in df['timestamp']]
    df.sort_values(by=['timestamp'], inplace=True)

    # df = pd.DataFrame({'timestamp':[1,2,3], 'stock_value':[1,2,3]})
    zoom_range = df['timestamp'][-zoom_last_values:]

    xrange = [min(zoom_range), max(zoom_range)]

    return df, xrange