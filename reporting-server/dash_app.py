import dash
import dash_core_components as dcc
import dash_html_components as html
from dash_html_components.H1 import H1
import plotly.express as px
import pandas as pd
from dash.dependencies import Input, Output
from datetime import datetime

from config import *
from data_generator import get_data

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, 
                external_stylesheets=external_stylesheets
                )


#run the chart for the first time
df, xrange = get_data()
fig = px.line(
    df, x="timestamp", y="stock_value", 
    title=f'Stock price, streaming last {zoom_last_values} values',
    range_x = xrange
    )

app.layout = html.Div(children=[
    html.H4(
        children="last update at "+datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        id='ts'
    ),
    dcc.Interval(
        id='interval-component', interval=1*10000,
        n_intervals=0
    ),
    dcc.Graph(
        id='example-graph',
        figure=fig
    )
])


@app.callback(
            [Output('ts', 'children'),
            Output('example-graph', 'figure'),],
            Input('interval-component', 'n_intervals')
            )
def update_metrics(n:int):

    df, xrange = get_data()
    fig = px.line(
        df, x="timestamp", y="stock_value", 
        title=f'Stock price, streaming last {zoom_last_values} values',
        range_x = xrange
    )

    return "last update at "+datetime.now().strftime("%Y-%m-%d %H:%M:%S"), fig
        

if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8050, debug=False)
