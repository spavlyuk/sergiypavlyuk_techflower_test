import json
import os
import logging

def main():
    try:
        with open ('auth.json') as f:
            db_creds = json.load(f)
        
        db_creds = db_creds[0]
        os.environ['DB_SERVER_TOKEN'] = db_creds['token']

        print ('new token has been set up')
        
    except FileNotFoundError:
        logging.error('Could not open json with db credentials"\
                        " or token key does not exist')

if __name__ == '__main__':
    main()
